package br.com.leandro.icarros.json.repository.http;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@AllArgsConstructor
public class RequestHttpRepository {

	private RequestHttpGetRepository requestHttpGetRepository;

	public <T, F> T get(Class<T> classResponse, String pathUri) {
		log.info("Efetuando a chamada GET para o serviço: {}", pathUri);
		return this.get(classResponse, pathUri, null);
	}

	public <T, F> T get(Class<T> classResponse, String pathUri, final F filter) {
		log.info("Efetuando a chamada GET para o serviÃ§o: {}", pathUri);
		return this.requestHttpGetRepository.get(classResponse, pathUri, filter);
	}
}

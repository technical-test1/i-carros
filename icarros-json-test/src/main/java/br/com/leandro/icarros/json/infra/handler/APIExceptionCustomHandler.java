package br.com.leandro.icarros.json.infra.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import br.com.leandro.icarros.json.infra.MessageConfig;
import br.com.leandro.icarros.json.infra.dto.ApiErrorDTO;
import br.com.leandro.icarros.json.infra.dto.ApiProperties;
import br.com.leandro.icarros.json.infra.dto.ResponseErrorDTO;
import br.com.leandro.icarros.json.infra.handler.exception.ApiException;
import br.com.leandro.icarros.json.infra.handler.exception.ApiHttpException;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class APIExceptionCustomHandler extends APIHandler {

	@Autowired
	public APIExceptionCustomHandler(MessageConfig messageConfig, ApiProperties properties) {
		this.messageConfig = messageConfig;
		this.properties = properties;
	}

	@ExceptionHandler(ApiException.class)
	public ResponseEntity<ResponseErrorDTO> handleAPIException(final ApiException ex, final ServletWebRequest request) {
		LOG.error(MESSAGE_DEFAULT_EXCEPTION, ex);

		return this.responseEntity(request, this.properties.getVersion(), this.apiErrorDTO(ex.getExceptionCodeEnum()),
				ex.getExceptionCodeEnum().getHttpStatus());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ResponseErrorDTO> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			WebRequest request) {

		LOG.error(MESSAGE_DEFAULT_EXCEPTION, ex);

		List<ApiErrorDTO> messages = new ArrayList<>();

		BindingResult bindingResult = ex.getBindingResult();
		List<FieldError> fes = bindingResult.getFieldErrors();

		for (FieldError fe : fes) {
			String defaultMessage = fe.getDefaultMessage();
			if (StringUtils.isNotBlank(defaultMessage)) {
				String message = this.messageConfig.message(defaultMessage);
				messages.add(ApiErrorDTO.builder().code(defaultMessage)
						.description(String.format("%s - %s", fe.getField(), message)).build());
			}
		}
		return this.responseEntity((ServletWebRequest) request, this.properties.getVersion(), messages,
				HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ExceptionHandler(ApiHttpException.class)
	public ResponseEntity<ResponseErrorDTO> handleApiHttpException(final ApiHttpException ex,
			final ServletWebRequest request) {
		LOG.error(MESSAGE_DEFAULT_EXCEPTION, ex);
		LOG.error("Resposta da requisiçao rest: ", ex.getResponse());

		return this.responseEntity(request, this.properties.getVersion(), this.apiErrorDTO(ex.getExceptionCodeEnum()),
				ex.getExceptionCodeEnum().getHttpStatus());
	}

}

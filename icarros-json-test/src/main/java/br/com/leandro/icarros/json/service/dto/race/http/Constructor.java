
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "constructorId", "url", "name", "nationality" })
@Getter
@Setter
public class Constructor implements Serializable {

	@JsonProperty("constructorId")
	private String constructorId;

	@JsonProperty("url")
	private String url;

	@JsonProperty("name")
	private String name;

	@JsonProperty("nationality")
	private String nationality;

}

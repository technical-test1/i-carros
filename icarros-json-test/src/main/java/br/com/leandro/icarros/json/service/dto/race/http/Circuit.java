
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "circuitId", "url", "circuitName", "Location" })
@Getter
@Setter
public class Circuit implements Serializable {

	@JsonProperty("circuitId")
	private String circuitId;

	@JsonProperty("url")
	private String url;

	@JsonProperty("circuitName")
	private String circuitName;

	@JsonProperty("Location")
	private Location location;

}

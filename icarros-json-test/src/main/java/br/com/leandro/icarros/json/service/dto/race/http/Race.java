
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "season", "round", "url", "raceName", "Circuit", "date", "time", "Results" })
@Getter
@Setter
public class Race implements Serializable {

	@JsonProperty("season")
	private Long season;

	@JsonProperty("round")
	private Long round;

	@JsonProperty("url")
	private String url;

	@JsonProperty("raceName")
	private String raceName;

	@JsonProperty("Circuit")
	private Circuit circuit;

	@JsonProperty("date")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate date;

	@JsonProperty("time")
	private String time;

	@JsonProperty("Results")
	private List<Result> results = null;

}

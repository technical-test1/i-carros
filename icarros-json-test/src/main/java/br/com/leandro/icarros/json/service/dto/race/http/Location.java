
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "lat", "long", "locality", "country" })
@Getter
@Setter
public class Location implements Serializable {

	@JsonProperty("lat")
	private Float latitude;

	@JsonProperty("long")
	private Float longitude;

	@JsonProperty("locality")
	private String locality;

	@JsonProperty("country")
	private String country;

}

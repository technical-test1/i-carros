package br.com.leandro.icarros.json.infra;


import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@Configuration
@OpenAPIDefinition(info = @Info(
    title = "Leandro Cunha - Teste tecnico",
    version = "${app.version}",
    description = "ICarros - Questão 6 – Consume WS-Rest"
))
public class SwaggerConfig {


}

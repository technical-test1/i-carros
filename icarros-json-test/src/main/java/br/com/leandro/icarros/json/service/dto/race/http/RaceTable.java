
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "season", "round", "Races" })
@Getter
@Setter
public class RaceTable implements Serializable {

	@JsonProperty("season")
	private Long season;

	@JsonProperty("round")
	private Long round;

	@JsonProperty("Races")
	private List<Race> races = null;

}

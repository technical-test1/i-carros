
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "driverId", "permanentNumber", "code", "url", "givenName", "familyName", "dateOfBirth",
		"nationality" })
@Getter
@Setter
public class Driver implements Serializable {

	@JsonProperty("driverId")
	private String driverId;

	@JsonProperty("permanentNumber")
	private Long permanentNumber;

	@JsonProperty("code")
	private String code;

	@JsonProperty("url")
	private String url;

	@JsonProperty("givenName")
	private String givenName;

	@JsonProperty("familyName")
	private String familyName;

	@JsonProperty("dateOfBirth")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateOfBirth;

	@JsonProperty("nationality")
	private String nationality;
}

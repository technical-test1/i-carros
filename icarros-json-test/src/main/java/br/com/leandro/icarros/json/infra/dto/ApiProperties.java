package br.com.leandro.icarros.json.infra.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties("app")
@Getter
@Setter
public class ApiProperties {

	private String version;

	private String url;

}

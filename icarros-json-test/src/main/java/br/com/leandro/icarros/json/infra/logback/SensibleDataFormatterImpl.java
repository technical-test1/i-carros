package br.com.leandro.icarros.json.infra.logback;

import java.util.Objects;

import lombok.NonNull;

public class SensibleDataFormatterImpl implements SensibleDataFormatter {

	private final String regex;

	public SensibleDataFormatterImpl(@NonNull String keyWord) {

		StringBuilder builder = new StringBuilder();
		for (char character : keyWord.toCharArray()) {
			builder.append("[");
			builder.append(Character.toString(character).toLowerCase());
			builder.append(Character.toString(character).toUpperCase());
			builder.append("]");
		}

		/**
		 *
		 * Log: 'Creating Product(id=1, name=Product X, ownerDocument=999.999.999-99, value=25.60)'
		 * Palavra chave: document e suas variações Document, document, dOcument etc...
		 *
		 * O Regex é separdo em 3 grupos
		 *
		 * 1º Grupo: Contém toda a mensagem até o delimitador final de atribuição desse campo, podendo ser : ou =
		 *
		 * Valor do grupo: Creating Product(id=1, name=Product X, ownerDocument=
		 *
		 * 2º Grupo: Contém o valor do campo até o delimitador final, podendo ser " ' ou ,
		 *
		 * Valor do grupo: 999.999.999-99
		 *
		 * 3º Grupo: Contém o resto da mensagem
		 *
		 * Valor do grupo:  , value=25.60
		 *
		 * Dado a separação do grupo, substitui o grupo 2 por ****
		 *
		 * 'Creating Product(id=1, name=Product X, ownerDocument=****, value=25.60)'
		 */
		this.regex = "(.*" + builder.toString() + "[a-zA-Z0-9]*[\"']? *[:=] *['\"]?)([^\"',)]+)(.*)";
	}

	@Override
	public boolean handle(String message) {
		return Objects.nonNull(message) && message.matches(regex);
	}

	@Override
	public String format(String message) {
		// Group $1 is before the value
		// Group $2 is the field value
		// Group $3 is after the value
		return message.replaceAll(regex, "$1****$3");
	}

}
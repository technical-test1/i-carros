package br.com.leandro.icarros.json.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.leandro.icarros.json.service.dto.race.http.Race;
import br.com.leandro.icarros.json.service.dto.race.response.RaceResultsResponse;

@Mapper(uses = { ResultMapper.class }, componentModel = "spring")
public abstract class RaceMapper implements ConvertMapper<RaceResultsResponse, Race> {

	@Override
	@Mapping(target = "positions", source = "results")
	@Mapping(target = "raceName", source = "raceName")
	@Mapping(target = "season", source = "season")
	public abstract RaceResultsResponse to(Race source);

}

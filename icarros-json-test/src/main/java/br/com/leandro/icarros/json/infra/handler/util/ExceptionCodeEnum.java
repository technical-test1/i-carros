package br.com.leandro.icarros.json.infra.handler.util;

import org.springframework.http.HttpStatus;

public enum ExceptionCodeEnum {

    INTERNAL_SERVER_ERROR( HttpStatus.INTERNAL_SERVER_ERROR),
    ERRO_AO_TENTAR_GRAVAR_CLIENTE( HttpStatus.INTERNAL_SERVER_ERROR),
    ERRO_AO_TENTAR_ALTERAR_CLIENTE( HttpStatus.INTERNAL_SERVER_ERROR),
    ERRO_AO_TENTAR_REMOVER_CLIENTE(  HttpStatus.INTERNAL_SERVER_ERROR),
    CLIENTE_INEXISTENTE( HttpStatus.INTERNAL_SERVER_ERROR),
    ERRO_AO_TENTAR_ALTERAR_PET( HttpStatus.INTERNAL_SERVER_ERROR),
    ERRO_AO_TENTAR_REMOVER_PET( HttpStatus.INTERNAL_SERVER_ERROR),
    CLIENTE_NAO_POSSUI_PET_CADASTRADO(HttpStatus.NO_CONTENT),
    PET_INEXISTENTE( HttpStatus.INTERNAL_SERVER_ERROR),
    ;

    private HttpStatus httpStatus;

    ExceptionCodeEnum( final HttpStatus httpStatus ) {
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return this.name();
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }
}

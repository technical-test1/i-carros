
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "units", "speed" })
@Getter
@Setter
public class AverageSpeed implements Serializable {

	@JsonProperty("units")
	private String units;

	@JsonProperty("speed")
	private Float speed;

}

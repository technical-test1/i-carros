package br.com.leandro.icarros.json.service.dto.race.response;

import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
public class QueryDetailsResponse {

	private String series;
	private Long season;
	private Long round;
	private Long results;
	private List<RaceResultsResponse> races;

}

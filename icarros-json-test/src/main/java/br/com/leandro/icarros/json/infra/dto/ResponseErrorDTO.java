package br.com.leandro.icarros.json.infra.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Tag(name = "ResponseErrorDTO")
@Schema(name = "ResponseErrorDTO", description = "Resposta de erro da requisição")
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
public class ResponseErrorDTO {

	@Schema(name = "error", description = "Mensagem de erro da requisição")
	private APIError error;

	public ResponseErrorDTO() {
		this(null);
	}

	public ResponseErrorDTO(APIError error) {
		this.error = error;
	}

}

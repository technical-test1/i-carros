package br.com.leandro.icarros.json.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.leandro.icarros.json.infra.dto.ResponseDTO;
import br.com.leandro.icarros.json.infra.dto.ResponseErrorDTO;
import br.com.leandro.icarros.json.service.QueryResultsService;
import br.com.leandro.icarros.json.service.dto.race.response.QueryDetailsResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/query-results")
@AllArgsConstructor
@Slf4j
@Tag(name = "QueryResultsController", description = "Questão 6 – Consume WS-Rest")
public class QueryResultsController {

	private QueryResultsService queryResultsService;

	@Operation(summary = "Result race F1", description = "F1", responses = {
			@ApiResponse(responseCode = "200", description = "Find with success"),
			@ApiResponse(responseCode = "204", description = "Empty returned", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "422", description = "Invalid body field", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ResponseErrorDTO.class))),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ResponseErrorDTO.class))) })
	@GetMapping("/v1/race-results")
	public ResponseEntity<ResponseDTO<QueryDetailsResponse>> findQueryResults() {
		log.info("Find Query Results {}");
		QueryDetailsResponse queryResultsResponse = this.queryResultsService.queryDetails();
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseDTO<>(queryResultsResponse));
	}

}

package br.com.leandro.icarros.json.infra.logback;

public interface SensibleDataFormatter {

	boolean handle(String message);

	String format(String message);

}

package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "rank", "lap", "Time", "AverageSpeed" })
@Getter
@Setter
public class FastestLap implements Serializable {

	@JsonProperty("rank")
	private Long rank;

	@JsonProperty("lap")
	private Long lap;

	@JsonProperty("Time")
	private LapTime time;

	@JsonProperty("AverageSpeed")
	private AverageSpeed averageSpeed;

}

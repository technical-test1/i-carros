package br.com.leandro.icarros.json.service.mapper;

import java.util.List;

public interface ConvertMapper<D, E> {

	D to(E source);

	List<D> to(List<E> sources);
}

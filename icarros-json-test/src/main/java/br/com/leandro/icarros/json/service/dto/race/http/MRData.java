
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "xmlns", "series", "url", "limit", "offset", "total", "RaceTable" })
@Getter
@Setter
public class MRData implements Serializable {

	@JsonProperty("xmlns")
	private String xmlns;

	@JsonProperty("series")
	private String series;

	@JsonProperty("url")
	private String url;

	@JsonProperty("limit")
	private Long limit;

	@JsonProperty("offset")
	private Long offset;

	@JsonProperty("total")
	private Long total;

	@JsonProperty("RaceTable")
	private RaceTable raceTable;

}

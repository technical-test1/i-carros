package br.com.leandro.icarros.json.service.mapper;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.leandro.icarros.json.service.dto.race.http.Driver;
import br.com.leandro.icarros.json.service.dto.race.http.Result;
import br.com.leandro.icarros.json.service.dto.race.http.Time;
import br.com.leandro.icarros.json.service.dto.race.response.RacePositionResponse;

@Mapper(uses = {}, componentModel = "spring")
public abstract class ResultMapper implements ConvertMapper<RacePositionResponse, Result> {

	@Override
	@Mapping(target = "constructor", expression = "java( source.getConstructor().getName() )")
	@Mapping(target = "driver", expression = "java( driveName( source.getDriver() ) )")
	@Mapping(target = "status", expression = "java( source.getStatus() )")
	@Mapping(target = "time", expression = "java( time( source.getTime() ) )")
	public abstract RacePositionResponse to(Result source);

	protected String driveName(Driver driver) {
		return driver.getGivenName().concat(StringUtils.SPACE).concat(driver.getFamilyName());
	}

	protected String time(Time time) {
		if (ObjectUtils.isEmpty(time)) {
			return StringUtils.EMPTY;
		}
		return StringUtils.defaultString(time.getTime(), StringUtils.EMPTY);
	}

}

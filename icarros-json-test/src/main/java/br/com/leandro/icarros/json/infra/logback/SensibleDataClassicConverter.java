package br.com.leandro.icarros.json.infra.logback;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class SensibleDataClassicConverter extends ClassicConverter {

	private final Collection<SensibleDataFormatter> formatters = new HashSet<>();

	public String formatMessage(String message) {
		String newMessage = String.copyValueOf(message.toCharArray());

		for (SensibleDataFormatter messageFormatter : formatters) {
			if (messageFormatter.handle(newMessage)) {
				newMessage = messageFormatter.format(newMessage);
			}
		}

		return newMessage;
	}

	@Override
	public void start() {
		super.start();
		Optional.ofNullable(getOptionList())
		.orElseGet(ArrayList::new)
		.forEach(item -> formatters.add(new SensibleDataFormatterImpl(item)));
	}

	@Override
	public String convert(ILoggingEvent iLoggingEvent) {
		return formatMessage(iLoggingEvent.getFormattedMessage());
	}

}
package br.com.leandro.icarros.json.service;

import br.com.leandro.icarros.json.service.dto.race.response.QueryDetailsResponse;

public interface QueryResultsService {

	QueryDetailsResponse queryDetails();
}

package br.com.leandro.icarros.json.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.leandro.icarros.json.service.dto.race.http.MRData;
import br.com.leandro.icarros.json.service.dto.race.response.QueryDetailsResponse;

@Mapper(uses = { RaceMapper.class }, componentModel = "spring")
public abstract class MRDataMapper implements ConvertMapper<QueryDetailsResponse, MRData> {

	@Autowired
	protected RaceMapper raceMapper;

	@Override
	@Mapping(target = "races", expression = "java( raceMapper.to( source.getRaceTable().getRaces() ) )")
	@Mapping(target = "results", source = "total")
	@Mapping(target = "round", expression = "java( source.getRaceTable().getRound() )")
	@Mapping(target = "season", expression = "java( source.getRaceTable().getSeason() )")
	public abstract QueryDetailsResponse to(MRData source);

}

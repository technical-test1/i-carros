package br.com.leandro.icarros.json.service.impl;

import org.springframework.stereotype.Service;

import br.com.leandro.icarros.json.infra.dto.ApiProperties;
import br.com.leandro.icarros.json.repository.http.RequestHttpRepository;
import br.com.leandro.icarros.json.service.QueryResultsService;
import br.com.leandro.icarros.json.service.dto.race.http.QueryResults;
import br.com.leandro.icarros.json.service.dto.race.response.QueryDetailsResponse;
import br.com.leandro.icarros.json.service.mapper.MRDataMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class QueryResultsServiceImpl implements QueryResultsService {

	private RequestHttpRepository requestHttpRepository;

	private ApiProperties properties;

	private MRDataMapper mRDataMapper;

	@Override
	public QueryDetailsResponse queryDetails() {
		log.info("Buscando as informação do race da F1");
		QueryResults response = this.requestHttpRepository.get(QueryResults.class, this.properties.getUrl());

		return this.mRDataMapper.to(response.getMRData());
	}

}

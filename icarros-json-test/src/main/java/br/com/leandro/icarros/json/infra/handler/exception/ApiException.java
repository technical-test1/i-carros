package br.com.leandro.icarros.json.infra.handler.exception;


import br.com.leandro.icarros.json.infra.handler.util.ExceptionCodeEnum;

public class ApiException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 5127042490049753890L;
	private final ExceptionCodeEnum exceptionCodeEnum;

	public ApiException(final ExceptionCodeEnum exceptionCodeEnum, final Throwable e) {
		super(e);
		this.exceptionCodeEnum = exceptionCodeEnum;
	}

	public ApiException(final ExceptionCodeEnum exceptionCodeEnum) {
		this(exceptionCodeEnum, null);
	}

	/**
	 * @return {@link ExceptionCodeEnum}
	 */
	public ExceptionCodeEnum getExceptionCodeEnum() {
		return this.exceptionCodeEnum;
	}
}

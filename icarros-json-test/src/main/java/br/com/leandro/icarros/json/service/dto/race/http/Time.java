
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "millis", "time" })
@Getter
@Setter
public class Time implements Serializable {

	@JsonProperty("millis")
	private Long millis;

	@JsonProperty("time")
	private String time;

}

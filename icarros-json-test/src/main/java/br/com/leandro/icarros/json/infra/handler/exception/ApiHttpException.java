package br.com.leandro.icarros.json.infra.handler.exception;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientResponseException;

import br.com.leandro.icarros.json.infra.handler.util.ExceptionCodeEnum;

public class ApiHttpException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final ExceptionCodeEnum exceptionCodeEnum;

	private final HttpStatus statusCode;

	private final String response;

	public ApiHttpException(ExceptionCodeEnum exceptionCodeEnum, final Throwable e) {
		super(e.getMessage(), e);
		this.exceptionCodeEnum = exceptionCodeEnum;
		this.response = responseStr(e);
		this.statusCode = responseHttpStatus(exceptionCodeEnum, e);
	}

	static HttpStatus responseHttpStatus(ExceptionCodeEnum exceptionCodeEnum, final Throwable e) {
		if (e instanceof HttpStatusCodeException) {
			return ((HttpStatusCodeException) e).getStatusCode();
		}
		return exceptionCodeEnum.getHttpStatus();
	}

	static String responseStr(Throwable e) {
		if (e instanceof RestClientResponseException) {
			return ((RestClientResponseException) e).getResponseBodyAsString();
		}
		return StringUtils.EMPTY;
	}

	public HttpStatus getStatusCode() {

		return this.statusCode;
	}

	public String getResponse() {
		return this.response;
	}

	public ExceptionCodeEnum getExceptionCodeEnum() {
		return this.exceptionCodeEnum;
	}
}

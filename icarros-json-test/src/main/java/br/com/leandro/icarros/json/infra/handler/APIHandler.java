package br.com.leandro.icarros.json.infra.handler;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.ServletWebRequest;

import br.com.leandro.icarros.json.infra.MessageConfig;
import br.com.leandro.icarros.json.infra.dto.APIError;
import br.com.leandro.icarros.json.infra.dto.ApiErrorDTO;
import br.com.leandro.icarros.json.infra.dto.ApiProperties;
import br.com.leandro.icarros.json.infra.dto.ResponseErrorDTO;
import br.com.leandro.icarros.json.infra.handler.util.ExceptionCodeEnum;

public class APIHandler {

	protected static final String MESSAGE_DEFAULT_EXCEPTION = "Uma Exception foi lancada:";

	protected static final Logger LOG = LoggerFactory.getLogger(APIHandler.class);

	protected ApiProperties properties;
	protected MessageConfig messageConfig;

	protected ApiErrorDTO apiErrorDTO(ExceptionCodeEnum exceptionCodeEnum) {

		return ApiErrorDTO.builder().code(exceptionCodeEnum.getCode())
				.description(this.messageConfig.message(exceptionCodeEnum)).build();
	}

	protected ResponseEntity<ResponseErrorDTO> responseEntity(final ServletWebRequest request, String version,
			ApiErrorDTO message, HttpStatus httpStatus) {
		return this.responseEntity(request, version, Arrays.asList(message), httpStatus);
	}

	protected ResponseEntity<ResponseErrorDTO> responseEntity(final ServletWebRequest request, String version,
			List<ApiErrorDTO> messages, HttpStatus httpStatus) {

		APIError message = new APIError(version, httpStatus.value(), messages, request.getRequest().getRequestURI(),
				request.getRequest().getMethod());
		return this.responseEntity(message);

	}

	protected ResponseEntity<ResponseErrorDTO> responseEntity(APIError messages) {
		LOG.info("{}", messages);
		ResponseErrorDTO body = new ResponseErrorDTO(messages);
		LOG.info("{}", body);
		return ResponseEntity.status(HttpStatus.valueOf(messages.getStatus())).body(body);
	}
}

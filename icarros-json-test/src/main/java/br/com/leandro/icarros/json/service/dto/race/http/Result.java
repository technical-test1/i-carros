
package br.com.leandro.icarros.json.service.dto.race.http;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "number", "position", "positionText", "points", "Driver", "Constructor", "grid", "laps", "status",
		"Time", "FastestLap" })
@Setter
@Getter
public class Result implements Serializable {

	@JsonProperty("number")
	private Long number;

	@JsonProperty("position")
	private Long position;

	@JsonProperty("positionText")
	private String positionText;

	@JsonProperty("points")
	private Long points;

	@JsonProperty("Driver")
	private Driver driver;

	@JsonProperty("Constructor")
	private Constructor constructor;

	@JsonProperty("grid")
	private Long grid;

	@JsonProperty("laps")
	private Long laps;

	@JsonProperty("status")
	private String status;

	@JsonProperty("Time")
	private Time time;

	@JsonProperty("FastestLap")
	private FastestLap fastestLap;
}

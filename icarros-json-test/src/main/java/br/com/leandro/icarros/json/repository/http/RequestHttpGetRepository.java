package br.com.leandro.icarros.json.repository.http;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.leandro.icarros.json.infra.handler.exception.ApiHttpException;
import br.com.leandro.icarros.json.infra.handler.util.ExceptionCodeEnum;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@AllArgsConstructor
@Slf4j
class RequestHttpGetRepository {
	private final RestTemplate restTemplate;

	private static final Integer MODIFIER_STATIC_FINAL = 26;

	protected <T, F> T get(Class<T> classResponse, String pathUri, final F filter) {
		log.info("Lista Recuperada");
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pathUri);
		Map<String, String> params = new HashMap<>();
		filter(pathUri, filter, builder, params);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		List<MediaType> accept = new ArrayList<>();
		accept.add(MediaType.ALL);
		headers.setAccept(accept);
		HttpEntity<String> requestEntity = new HttpEntity<>("parameters", headers);
		log.info("Iniciando o RestTemplate");

		try {
			log.info("Request: {}", builder.buildAndExpand(params).toUri());

			ResponseEntity<T> response = this.restTemplate.exchange(builder.buildAndExpand(params).toUri(),
					HttpMethod.GET, requestEntity, classResponse);
			log.info("Resquest executado");

			return response.getBody();
		} catch (HttpClientErrorException e) {
			throw new ApiHttpException(ExceptionCodeEnum.INTERNAL_SERVER_ERROR, e);
		}
	}

	static <F> void filter(String pathUri, final F filter, UriComponentsBuilder builder, Map<String, String> params) {

		if (filter == null) {
			return;
		}
		Field[] fields = filter.getClass().getDeclaredFields();
		try {
			for (Field field : fields) {
				Boolean isAccessible = field.canAccess(field);
				field.setAccessible(Boolean.TRUE);
				Integer modifier = field.getModifiers();
				Object value = field.get(filter);

				if (MODIFIER_STATIC_FINAL.equals(modifier) || ObjectUtils.isEmpty(value)) {
					continue;
				}

				String paramName = field.getName();

				String attrib = String.format("{%s}", paramName);

				addValueRequest(pathUri, builder, params, value, paramName, attrib);

				field.setAccessible(isAccessible);
			}
		} catch (IllegalAccessException e) {
			throw new ApiHttpException(ExceptionCodeEnum.INTERNAL_SERVER_ERROR, e);
		}

	}

	static void addValueRequest(String pathUri, UriComponentsBuilder builder, Map<String, String> params, Object value,
			String paramName, String attrib) {

		if (value instanceof List) {
			List<?> values = (List<?>) value;
			for (Object object : values) {
				builder.queryParam(paramName, String.valueOf(object));
			}
			return;
		}

		if (pathUri.contains(attrib)) {
			params.put(paramName, String.valueOf(value));
			return;
		}
		builder.queryParam(paramName, String.valueOf(value));
	}
}

package br.com.leandro.icarros.json.service.dto.race.response;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
public class RacePositionResponse {

	private Long position;

	private Long number;

	private String driver;

	private String constructor;

	private Long laps;

	private Long grid;

	private String time;

	private String status;

	private Long points;

}

package br.com.leandro.icarros.json;

import static org.junit.Assert.assertNotNull;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.leandro.icarros.json.infra.dto.ResponseDTO;
import br.com.leandro.icarros.json.service.dto.race.response.QueryDetailsResponse;

@ActiveProfiles(profiles = { "test" })
//@TestPropertySource(locations = "classpath:application-test.yml")
@SpringBootTest(classes = { Application.class })
@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class QueryResultsControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void test_query_detail_success() throws Exception {

		MvcResult result = this.mockMvc
				.perform(MockMvcRequestBuilders.get("/api/query-results/v1/race-results")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseDTO<QueryDetailsResponse> response = this.objectMapper.readValue(contentAsString,
				new TypeReference<ResponseDTO<QueryDetailsResponse>>() {
				});
		assertNotNull(response.getData());

		System.out.println(response.getData());
	}

}

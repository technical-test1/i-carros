# Orientações

Neste desafio procurei desenvolver o mais próximo da minha vida profissional. Utilizei algumas frameworks que auxiliam muito no desenvolvimento e na produtivida.

# Questão 6 – Consume WS-Rest

Desenvolvido um microservico que consome um endpoint rest e transforma as informações para o formato espefico de um relatório.

Dentro da pasta "icarros-json-test" seguir os seguntes passos:

### Compilar o projeto:
 - mvn clean install

Precisa de internet, pois o teste executado acessará a url para obter o response da conexão.

### Executando e acessando o swagger do projeto:
 - java -jar target/icarros-json-test.jar
 - Swagger: http://localhost:8082/swagger-ui.html#/

### Link para acessar via browse (local):
 - GET
 - http://localhost:8082/api/query-results/v1/race-results
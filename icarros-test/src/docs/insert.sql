insert into TB_CUSTOMER values(1 , 'John Victor' );
insert into TB_CUSTOMER values(2 , 'DeathStar' );
commit;
insert into TB_ORDER (CD_ORDER ,DH_ORDER , CD_CUSTOMER) values (1, now() , 1 );
insert into TB_ORDER (CD_ORDER ,DH_ORDER , CD_CUSTOMER) values (2, now() , 2 );
insert into TB_ORDER (CD_ORDER ,DH_ORDER , CD_CUSTOMER) values (3, now() , 1 );
insert into TB_ORDER (CD_ORDER ,DH_ORDER , CD_CUSTOMER) values (4, now() , 2 );
commit;

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 1 , 2 , 20.00, 1);

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 2 , 1 , 120.01, 1);

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 3 , 5 , 129.01, 1);

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 4 , 2 , 20.00, 2);

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 5 , 1 , 20.01, 2);

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 6 , 5 , 17.01, 3);
     
insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 7 , 2 , 22.00, 3);

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 7 , 1 , 201.01, 3);

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 9 , 5 , 29.01, 4);
     
insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 6 , 5 , 17.01, 4);
     
insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 7 , 2 , 22.00, 4);

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 8 , 1 , 201.01, 4);

insert into TB_ORDER_ITEM (CD_ORDER_ITEM, NU_QUANT_ITEM, VL_PRICE_UNIT, CD_ORDER ) 
     values ( 9 , 5 , 29.01, 3);
commit;
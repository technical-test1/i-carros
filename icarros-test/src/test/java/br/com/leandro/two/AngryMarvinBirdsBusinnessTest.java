package br.com.leandro.two;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class AngryMarvinBirdsBusinnessTest {

	@InjectMocks
	private AngryMarvinBirdsBusinness angryMarvinBirdsService;

	@Before
	public void setup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void test_angry_marvin_birds_service_result() {

		this.angryMarvinBirdsService.addPoint(0, 1);
		this.angryMarvinBirdsService.addPoint(1, 0);
		this.angryMarvinBirdsService.addPoint(2, 3);
		this.angryMarvinBirdsService.addPoint(3, 2);
		this.angryMarvinBirdsService.addPoint(4, 1);

		String[] result = this.angryMarvinBirdsService.result();

		System.out.println(String.format("Point %s --> Result: %s", String.valueOf(1), String.valueOf(result[0])));
		System.out.println(String.format("Point %s --> Result: %s", String.valueOf(0), String.valueOf(result[1])));
		System.out.println(String.format("Point %s --> Result: %s", String.valueOf(3), String.valueOf(result[2])));
		System.out.println(String.format("Point %s --> Result: %s", String.valueOf(2), String.valueOf(result[3])));
		System.out.println(String.format("Point %s --> Result: %s", String.valueOf(1), String.valueOf(result[4])));

		assertEquals(result[0], "*--");
		assertEquals(result[1], "---");
		assertEquals(result[2], "***");
		assertEquals(result[3], "**-");
		assertEquals(result[4], "*--");
	}

}

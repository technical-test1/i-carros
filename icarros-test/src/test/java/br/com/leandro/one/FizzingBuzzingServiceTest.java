package br.com.leandro.one;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class FizzingBuzzingServiceTest {

	@InjectMocks
	private FizzingBuzzingService fizzingBuzzingService;

	@Before
	public void setup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void test_fizzing_buzzing_service_execute() {
		Boolean execute = Boolean.TRUE;
		fizzingBuzzingService.execute();
		assertTrue(execute);
	}

}

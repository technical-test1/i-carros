package br.com.leandro.three;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class CreepyHoursServiceTest {

	@InjectMocks
	private CreepyHoursService creepyHoursService;

	@Before
	public void setup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void test_creepy_hours_service_beep() {
		String[] hours = new String[3];
		hours[0] = "11:00";
		hours[1] = "13:13";
		hours[2] = "10:00";
		long beep = this.creepyHoursService.beep(hours);

		for (String hour : hours) {
			System.out.println(hour);
		}
		System.out.println(beep);
		assertEquals(2L, beep);

		hours = new String[3];
		hours[0] = "12:11";
		hours[1] = "13:22";
		hours[2] = "10:33";
		beep = this.creepyHoursService.beep(hours);
		for (String hour : hours) {
			System.out.println(hour);
		}
		System.out.println(beep);

		assertEquals(0L, beep);

		hours = new String[7];
		hours[0] = "12:00";
		hours[1] = "13:13";
		hours[2] = "10:00";
		hours[3] = "10:01";
		hours[4] = "10:02";
		hours[5] = "10:03";
		hours[6] = "10:04";
		beep = this.creepyHoursService.beep(hours);
		for (String hour : hours) {
			System.out.println(hour);
		}
		System.out.println(beep);

		assertEquals(5L, beep);
	}
}

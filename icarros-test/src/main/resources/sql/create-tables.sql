Create table TB_CUSTOMER (
	CD_CUSTOMER numeric NOT NULL  UNIQUE ,
	NM_CUSTOMER Varchar (200) NOT NULL ,
 Constraint pk_TB_CUSTOMER primary key (CD_CUSTOMER) 
) 
;
Create table TB_ORDER (
	CD_ORDER numeric NOT NULL  UNIQUE ,
	DH_ORDER Date NOT NULL ,
	CD_CUSTOMER numeric NOT NULL ,
 Constraint pk_TB_ORDER primary key (CD_ORDER) 
) 
;
Create table TB_ORDER_ITEM (
	CD_ORDER_ITEM numeric NOT NULL  UNIQUE ,
	NU_QUANT_ITEM numeric NOT NULL ,
	VL_PRICE_UNIT numeric(10,2) NOT NULL ,
	CD_ORDER numeric NOT NULL ,
 Constraint pk_TB_ORDER_ITEM primary key (CD_ORDER_ITEM) 
) 
;
Alter table TB_ORDER add Constraint FK_CUSTOMER_ORDER foreign key (CD_CUSTOMER) references TB_CUSTOMER (CD_CUSTOMER) 
;
Alter table TB_ORDER_ITEM add Constraint FX_ORDER_ORDER_ITEM foreign key (CD_ORDER) references TB_ORDER (CD_ORDER) 
;

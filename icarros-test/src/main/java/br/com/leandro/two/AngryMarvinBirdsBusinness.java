package br.com.leandro.two;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;

/***
 *
 * @author Leandro Cunha
 *
 *         Marvin is addicted to 'Angry Birds�, which became huge in the
 *         vicinities of Betelgeuse. The game has multiple stages, and for each
 *         stage the player can gain between 0 and 3 stars, inclusive. You are
 *         given a String[] result, containing Marvin's results. For each stage,
 *         result[i] contains an element that specifies Marvin's result in that
 *         stage. For example, if he got 0 stars in stage i, result[i] would be
 *         �---�. For 1 star, result[i] would be �*--�, and so on. Return the
 *         total number of stars Marvin has at the moment.
 *
 */
public final class AngryMarvinBirdsBusinness {

	private static final int LIMIT_STAR_FASE = 3;
	private List<Integer> fasesPoint = null;

	public AngryMarvinBirdsBusinness() {
		this.fasesPoint = new LinkedList<>();
	}

	public void addPoint(final Integer fase, final Integer point) {
		this.fasesPoint.add(fase, point);
	}

	public String[] result() {

		String[] result = new String[this.fasesPoint.size()];

		IntStream.range(0, this.fasesPoint.size()).forEach(init -> {
			result[init] = StringUtils.truncate("***", this.fasesPoint.get(init))
					.concat(StringUtils.truncate("---", LIMIT_STAR_FASE - this.fasesPoint.get(init)));
		});

		return result;
	}
}

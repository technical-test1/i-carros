package br.com.leandro.four;

import java.util.stream.IntStream;

import br.com.leandro.four.dto.Points;
import br.com.leandro.four.dto.Result;

/***
 *
 * @author Leandro Cunha
 *
 *         In soccer leagues, the winner of a match is awarded with 3 points and
 *         the loser 0 points. In case of a tie, both teams are awarded with 1
 *         point each. Given int[] wins and int[] ties, and knowing that the
 *         i'th elements of wins and ties correspond to the number of wins and
 *         ties respectively for team i; return the number of points the
 *         champion team achieved in this league. For inputs wins = {1,0,3} and
 *         ties = {2, 2, 0}, the result would be 9 (the team at i=2 would win
 *         this championship).
 */
public class WeAreTheChampionsMyFriendService {

	public Result championshipResult(Points points) {

		ProcessResultBusinness processResultBusinness = ProcessResultBusinness.newInstance(points.getTeamsQuantity());

		points.getTeamsPoints().entrySet().forEach(entry -> {
			IntStream.range(0, entry.getValue().length).forEach(index -> {
				processResultBusinness.process(index, entry.getValue()[index] * entry.getKey().getPoint());
			});
		});

		return Result.builder().result(processResultBusinness.getResult()).champion(processResultBusinness.getPosition()).build();
	}
}

package br.com.leandro.four.dto;

public class Result {

	private int[] result;

	private int champion;

	private Result( Builder builder ) {
		this.result = builder.result;
		this.champion = builder.champion;
	}

	public int[] getResult() {
		return result;
	}

	public int getChampion() {
		return champion;
	}

	public int getChampionPoint() {
		return result[champion];
	}

	@Override
	public String toString() {
		return String.format( "The champion team is %s with %s points." , String.valueOf(champion), String.valueOf(getChampionPoint()) );
	}


	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private int[] result = null;
		private int champion = 0;

		public Builder result( int[] result) {
			this.result  = result ;
			return this;
		}

		public Builder champion( int champion) {
			this.champion  = champion ;
			return this;
		}

		public Result build() {
			return new Result(this);
		}
	}
}

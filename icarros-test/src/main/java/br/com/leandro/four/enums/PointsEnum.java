package br.com.leandro.four.enums;

public enum PointsEnum {

	WINS(3),
	TIES(1),
	;

	private int point;

	PointsEnum(int point ){
		this.point = point;
	}

	public int getPoint() {
		return point;
	}

}

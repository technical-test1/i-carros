package br.com.leandro.four;

class ProcessResultBusinness {

	private int[] result;
	private Integer maxPoint = 0;
	private Integer position = 0;

	static ProcessResultBusinness newInstance(int teamsQuantity) {
		return new ProcessResultBusinness(teamsQuantity);
	}

	private ProcessResultBusinness(int teamsQuantity) {
		this.result = new int[teamsQuantity];
	}

	void process(int index, int point) {
		this.result[index] += point;
		if (this.result[index] > this.maxPoint) {
			this.maxPoint = this.result[index];
			this.position = index;
		}
	}

	Integer getPosition() {
		return this.position;
	}

	int[] getResult() {
		return this.result;
	}

}

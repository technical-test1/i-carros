package br.com.leandro.four.dto;

import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;

import br.com.leandro.four.enums.PointsEnum;

public class Points {

	private Map<PointsEnum, int[]> teamsPoints = new HashedMap<>();

	private int teamsQuantity;

	private Points(Builder builder ){
		this.teamsPoints.put(PointsEnum.TIES, builder.ties);
		this.teamsPoints.put(PointsEnum.WINS, builder.wins);
		this.teamsQuantity = builder.teamsQuantity;
	}

	public Map<PointsEnum, int[]> getTeamsPoints() {
		return teamsPoints;
	}

	public int getTeamsQuantity() {
		return teamsQuantity;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private int[] ties = new int[0];
		private int[] wins = new int[0];

		private int teamsQuantity = 0;

		public Builder wins(int[] wins) {
			this.wins = wins;
			this.teamsQuantity = wins.length;
			return this;
		}

		public Builder ties(int[] ties) {
			this.ties = ties;
			this.teamsQuantity = ties.length;
			return this;
		}

		public Points build() {
			return new Points(this);
		}
	}
}

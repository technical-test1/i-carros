package br.com.leandro.one;

import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/***
 *
 * @author Leandro Cunha
 *
 *         Write a program that prints the numbers from 1 to 100. But for
 *         multiples of three print "Fizz" instead of the number and for
 *         multiples of five print "Buzz". For numbers which are multiples of
 *         both three and five print "FizzBuzz".
 *
 */

public class FizzingBuzzingService {

	private static final Integer MULTIPLE_THREE = 3;
	private static final Integer MULTIPLE_FIVE = 5;
	private static final Integer LIMIT = 100;
	private static final Integer INITIAL = 1;

	public void execute() {

		IntStream.range(INITIAL, LIMIT).forEach(number -> {
			StringBuilder messageBuider = new StringBuilder();

			if ((number % MULTIPLE_THREE) == NumberUtils.INTEGER_ZERO) {
				messageBuider.append("Fizz");
			}
			if ((number % MULTIPLE_FIVE) == NumberUtils.INTEGER_ZERO) {
				messageBuider.append("Buzz");
			}
			this.print(messageBuider.toString());
		});
	}

	private void print(String message) {
		if (StringUtils.isNotBlank(message)) {
			System.out.println(message);
		}
	}
}

package br.com.leandro.three;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;

/***
 *
 * @author Leandro Cunha
 *
 * Suppose that we're given a moment of time written as HH:MM, where HH is the
 * hour and MM is the minutes, and a clock that beeps every time a creepy
 * moment happens. Let's say that a moment is considered creepy if it is
 * formatted as AB:AB, AA:BB, AB:BA or AA:AA. You are given a String[]
 * moments, where each element represents a single moment of time. Return how
 * many times the clock beeped. For example, for an input
 * {"11:00","13:13","10:00"}, the result would be 2.
 *
 */
public class CreepyHoursService {

	private static final int HOUR = 0;
	private static final int MINUTE = 1;

	public Long beep( String[] hours ) {
		Long beep = NumberUtils.LONG_ZERO;
		for (String time : hours) {
			if( isBeepCreepy(time)) {
				beep++;
			}
		}
		return beep;
	}

	private Boolean isBeepCreepy(String time) {
		String[] hourMinute = time.split(":");
		Boolean hour   = BooleanUtils.isFalse(isAA(hourMinute[HOUR]));
		Boolean minute = isAA(hourMinute[MINUTE]);
		return BooleanUtils.isFalse( hour &&  minute ) ;
	}

	private Boolean isAA(String moment ) {
		String digit1 = moment.substring(0, 1);
		String digit2 = moment.substring(1, 2);
		return digit1.equals(digit2);
	}
}

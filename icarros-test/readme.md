# Orientações

Para cada questão foi desenvolvida a classe de implementação e a classe de teste, conforme descrito abaixo.

# Questão 1 - Fizzing 'n buzzing

 - pacote: br.com.leandro.one
 - Implementação: FizzingBuzzingService.java
 - Teste: FizzingBuzzingServiceTest.java

# Questão 2 - Angry Marvin Birds

 - pacote: br.com.leandro.two
 - Implementação: AngryMarvinBirdsBusinness.java
 - Teste: AngryMarvinBirdsBusinnessTest.java

# Questão 3 - Creepy hours

 - pacote: br.com.leandro.three
 - Implementação: CreepyHoursService.java
 - Teste: CreepyHoursServiceTest.java

# Questão 4 - We are the champions, my friend

 - pacote: br.com.leandro.four
 - Implementação: WeAreTheChampionsMyFriendService.java
 - Teste: WeAreTheChampionsMyFriendServiceTest.java

# Questão 5 - Venting the costs

Foi utilizado o banco de dados PostgreSQL.

 - pacote: resources/sql
 - Scripts:
        - create-tables.sql: Cria as tabelas
        - insert.sql: Insere a carga
        - response-select.sql: Select da resposta
        - Venting the costs.png: Imagem do Modelo